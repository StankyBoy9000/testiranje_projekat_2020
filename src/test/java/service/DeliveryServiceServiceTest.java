/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.DeliveryServiceDAO;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marko
 */
public class DeliveryServiceServiceTest {
    private static DeliveryServiceService dss;
    private static DeliveryServiceDAO ddao;
    private static int testNumber;
    public DeliveryServiceServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        ddao = new DeliveryServiceDAO();
        testNumber = 0;
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dss = new DeliveryServiceService();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    @Test
    public void testRegister() {
        testNumber++;
        boolean result = dss.register("DeliveryService003",200.0f,50.0f);
        assertEquals(true, result);
        assertEquals(testNumber,(int)ddao.getOne(testNumber).getId());
        assertEquals(200.0f,(float)ddao.getOne(testNumber).getStartingPrice(),0.2);
        assertEquals(50.0f,(float)ddao.getOne(testNumber).getPricePerKilometer(),0.2);
        assertEquals("DeliveryService003", ddao.getOne(testNumber).getName());
    }
    
    @Test
    public void testRegisterNegativePricePerKm() {
        
        boolean result = dss.register("DeliveryService",-1.0f,200.0f);
        assertEquals(false, result);
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if( x.getPricePerKilometer() == -1.0f){
                result = false;
            }
        }
        if(!result){
            testNumber++;
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testRegisterZeroPricePerKm() {
        
        boolean result = dss.register("DeliveryService",0.0f,200.0f);
        assertEquals(false, result);
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if( x.getPricePerKilometer() == 0.0f){
                result = false;
            }
        }
        if(!result){
            testNumber++;
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testRegisterNegativeStartingPrice() {
        
        boolean result = dss.register("DeliveryService",200.0f,-1.0f);
        assertEquals(false, result);
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if( x.getStartingPrice() == -1.0f){
                result = false;
            }
        }
        if(!result){
            testNumber++;
        }
        assertEquals(true, result);
    }

    @Test
    public void testRegisterZeroStartingPrice() {
        
        boolean result = dss.register("DeliveryService",200.0f,0.0f);
        assertEquals(false, result);
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if( x.getStartingPrice() == 0.0f){
                result = false;
            }
        }
        if(!result){
            testNumber++;
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testRegisterWithoutName() {
        
        boolean result = dss.register("",200.0f,0.0f);
        assertEquals(false, result);
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if( x.getName().equals("")){
                result = false;
            }
        }
        if(!result){
            testNumber++;
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testDeleteDeliveryService() {
        testNumber++;
        DeliveryService newDS =  new DeliveryService(testNumber, "Service001", 30.0f, 35.0f);
        ddao.insertOne(newDS);
        boolean result = dss.deleteDeliveryService(newDS);
        assertEquals(true, result);
        
        for(DeliveryService x: ddao.getAll()){
            if(x.getId() == testNumber){
                result = false;
            }
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testDeleteNonExistingDeliveryService() {
        boolean result = dss.deleteDeliveryService(new DeliveryService(999, "UniqueName001", 1.0f,1.0f));
        assertEquals(false, result);
        
        result = true;
        for(DeliveryService x: ddao.getAll()){
            if(x.getName().equals("UniqueName001")){
                result = false;
            }
        }
        assertEquals(true, result);
    }
    
    @Test
    public void testUpdateInfoWithNullID() {
        testNumber++;
        DeliveryService newDS = new DeliveryService(null, "newDs", 20.0f, 21.0f);
        ddao.insertOne(newDS);
        
        boolean result = dss.updateInfo(newDS, "newDs", 25.0f, 30.0f);
        assertEquals(false, result);
    }
    
    @Test
    public void testUpdateNumbers() {
        testNumber++;
        DeliveryService newDS = new DeliveryService(testNumber, "newDs001", 20.0f, 21.0f);
        ddao.insertOne(newDS);
        
        boolean result = dss.updateInfo(newDS, "newDs001", 25.0f, 30.0f);
        assertEquals(true, result);
        
        DeliveryService dbDS = ddao.getOne(testNumber);
        assertEquals(25.0f, dbDS.getPricePerKilometer(), 0.2);
        assertEquals(30.0f, dbDS.getStartingPrice(), 0.2);
    }
    
        public void testUpdateName() {
        testNumber++;
        DeliveryService newDS = new DeliveryService(testNumber, "newDs003", 20.0f, 21.0f);
        ddao.insertOne(newDS);
        
        boolean result = dss.updateInfo(newDS, "newDs002", 25.0f, 30.0f);
        assertEquals(true, result);
        
        DeliveryService dbDS = ddao.getOne(testNumber);
        assertEquals("newDs002", dbDS.getName());
    }
    
}


